call plug#begin('~/.config/nvim/plugged')

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'tpope/vim-unimpaired'
Plug 'qpkorr/vim-bufkill'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'catppuccin/nvim', { 'as': 'catppuccin' }
Plug 'kdheepak/lazygit.nvim'

call plug#end()

" not compatible with vi
set nocp

" line number
set nu

" check filetype
filetype plugin indent on

" set tabs and shift
set ts=2
set sw=2
set sts=2
set expandtab

" syntax and color scheme
syntax on
syntax enable
set background=dark
colorscheme catppuccin-mocha

" indent
set autoindent

" highlight boundary column
set cc=80

" show cursor position
" may be bad when not using solarized scheme
set cursorline
set cursorcolumn

" fold
set foldmethod=syntax

" search
set ic
set hlsearch
" search for visual seleced string
vnoremap // y/<C-R>"<CR>
" turn off highlight of matched string
nnoremap <LEADER>h :noh<CR>

" ignore files
set wig=*.o,*.exe

" map key for telescope
nnoremap <C-p> <cmd>Telescope find_files<cr>

" key mapping for moving across splits
nmap <silent> <c-h> :wincmd h<CR>
nmap <silent> <c-j> :wincmd j<CR>
nmap <silent> <c-k> :wincmd k<CR>
nmap <silent> <c-l> :wincmd l<CR>

" Return from terminal mode to normal
tnoremap <Esc> <c-\><c-n>
" key mapping for moving across terminal splits
tnoremap <c-h> <c-\><c-n><c-w>h
tnoremap <c-j> <c-\><c-n><c-w>j
tnoremap <c-k> <c-\><c-n><c-w>k
tnoremap <c-l> <c-\><c-n><c-w>l
