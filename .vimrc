" not compatible with vi
set nocp

" line number
set nu

" load plugins
source ~/.vim/plugins.vim

" check filetype
filetype plugin indent on

" save files and quit
nnoremap <C-q> :x<CR>
nnoremap <C-s> :w<CR>
inoremap <C-q> <ESC>:x<CR>
inoremap <C-s> <ESC>:w<CR>

" set tabs and shift
set ts=2
set sw=2
set sts=2
set expandtab

" syntax and color scheme
syntax on
syntax enable
set background=dark
set t_Co=256
let g:solarized_termcolors=256
colorscheme solarized

" indent
set autoindent

" highlight boundary column
set cc=80

" show cursor position
" may be bad when not using solarized scheme
set cursorline
set cursorcolumn

" fold
set foldmethod=syntax

" search
set ic
set hlsearch
" search for visual seleced string
vnoremap // y/<C-R>"<CR>
" turn off highlight of matched string
nnoremap <LEADER>h :noh<CR>

" ignore files
set wig=*.o,*.exe
