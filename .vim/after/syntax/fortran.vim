" Vim syntax file
" Language:    Fortran, MPI
" Author:      Hengjie Wang ( Frank )
" Last Update: Feb 07 2016

syn match mpi /\<MPI_[A-Za-z_]*(/me=e-1
syn match symbols /[%:]/ 
syn match parentheses /[()\[\]]/
setlocal iskeyword+=.
syn keyword operators .x.
syn region openmp start=/!$.\{-}/ end=/[^\&]$/

hi mpi ctermfg=219
hi symbols ctermfg=111
hi Search ctermbg=yellow
hi operators ctermfg=yellow
hi parentheses ctermfg=darkmagenta
hi openmp ctermfg=red
