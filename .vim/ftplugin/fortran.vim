"--------------------abbreviation--------------------
iab subr SUBROUTINE
iab func FUNCTION
iab esubr END SUBROUTINE
iab efunc END FUNCTION
iab im IMPLICIT NONE
iab para PARAMETER
iab char CHARACTER
iab al ALLOCATE
iab dal DEALLOCATE
iab alb ALLOCATABLE
iab prd PROCEDURE
iab ass ASSOCIATE
iab asd ASSOCIATED
iab mcw MPI_COMM_WORLD


" tab
set sw=2
set ts=2
set expandtab

" fold
set foldmethod=syntax
let fortran_fold=1

set cc=80
hi ColorColunm ctermbg=243

source ~/.vim/plugins/CapFtn.vim

function! InsertHeader(str)

	let line_num = line(".")

	" insert header
	if a:str == 'hm'
		exe 'r' . "$HOME/.vim/template/fortran_module_header.txt"
	elseif a:str == 'hp'
		exe 'r' . "$HOME/.vim/template/fortran_procedure_header.txt"
	endif

	" find the line containing 'PURPOSE' and set the cursor at 'E'
	" the line should be within 5 lines
	let line_num = search('PURPOSE', 'e', line_num+5)

	" move cursor to the end of line
	" the line should end at ':' without trailing space
	exe "normal $"

endfunction

imap <LEADER>hm <ESC>:call InsertHeader('hm')<CR>a
imap <LEADER>hp <ESC>:call InsertHeader('hp')<CR>a


function! ToggleNerdtreeTagbar()

	" check if NERDTree and Tagbar are opened
	let NERDTree_close = (bufwinnr('NERD_tree') == -1)
	let Tagbar_close   = (bufwinnr('__Tagbar__') == -1)

	TagbarToggle
	NERDTreeToggle

	if NERDTree_close && Tagbar_close
		wincmd K
		wincmd b
		wincmd L
		wincmd h
		exe 'vertical resize 40'
	endif

endfunction

nmap <C-e> :call ToggleNerdtreeTagbar()<CR>


function! LastUpdate()

	let cursor_pos = getpos(".")

	if line("$") > 40
		let LastUpdateLine = 40
	else
		let LastUpdateLine = line("$")
	endif

  exe "normal 1G"
	let LastUpdateLine = search('LAST UPDATE', '', LastUpdateLine) 

	if LastUpdateLine > 0
		"exe '1,' . LastUpdateLine . 
		  "\ 'g/LAST UPDATE/s/\(LAST UPDATE :\).*/\1 ' . 
		  "\ strftime("%b %d %Y")
		exe LastUpdateLine . ',' . LastUpdateLine . 
		  \ 's/\(LAST UPDATE :\).*/\1 ' . strftime("%b %d %Y")
	endif

	call setpos('.', cursor_pos)

endfunction 

"autocmd BufWritePre,FileWritePre *.f90 call LastUpdate()
