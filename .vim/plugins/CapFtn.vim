" Capitalize Fortran Keywords
" This may not work properly when there is sth like quotes with exclamation in
" comments.
" Nov 20 2015 

if exists("g:loaded_CapFtn2")
	finish
endif
let g:loaded_CapFtn2 = 1

let Ftn_IO = ['backspace', 'close', 'endfile', 'inquire', 'open', 'print', 'read', 
		     \'rewind', 'write', 'access', 'blank', 'direct', 'exist', 'file', 
			 \'fmt', 'form', 'format', 'formatted', 'iostat', 'name', 'named', 
			 \'nextrec', 'number', 'opened', 'rec', 'recl', 'sequential', 'status', 
			 \'unformatted', 'unit']

let Ftn_F = ['abs', 'acos', 'aimag', 'aint', 'anint', 'asin', 'atan', 'atan2', 'char', 
		    \'cmplx', 'cos', 'cosh', 'exp', 'ichar', 'index', 'int', 'log', 'log10', 
		    \'max', 'min', 'nint', 'sign', 'sin', 'sinh', 'sqrt', 'tan', 'tanh', 'all', 
		    \'allocated', 'any', 'associated', 'bit_size', 'btest', 'ceiling', 'count', 
		    \'cshift', 'date_and_time', 'dot_product', 'epsilon', 'exponent', 'floor', 
		    \'huge', 'iand', 'ieor', 'ior', 'lbound', 'len_trim', 'matmul', 'maxloc', 
		    \'maxval', 'merge', 'minloc', 'minval', 'nearest', 'present', 'product', 
		    \'random_number', 'random_seed', 'range', 'repeat', 'reshape', 'scale', 'scan',
		    \'selected_int_kind', 'selected_real_kind', 'set_exponent', 'shape', 'size',
		    \'spread', 'sum', 'system_clock', 'tiny', 'transpose', 'trim', 'ubound', 
		    \'verify', 'command_argument_count', 'get_command', 'get_command_argument', 
		    \'get_environment_variable', 'move_alloc', 'new_line', 'selected_char_kind',
		    \'allocate', 'deallocate', 'nullify']

let Ftn_Structure = ['module', 'program', 'procedure', 'subroutine', 'function', 'do', 
		            \'interface', 'associate', 'operator', 'result', 'call', 'contains', 
					\'generic', 'extermal', 'import', 'assign', 'pause', 'implicit', 
					\'none', 'private', 'public', 'intent', 'optional', 'present', 'use', 
					\'only', 'if', 'end', 'else', 'then', 'select', 'case', 'default', 
					\'forall', 'for', 'where', 'elsewhere', 'while', 'cycle', 'stop', 
					\'exit', 'include', 'return']

let Ftn_Type = ['real', 'logical', 'integer', 'character', 'type', 'class', 'pointer', 
		       \'target', 'allocatable', 'abstract', 'deferred', 'extends', 'null']

let s:FortranWords=[]
call extend(s:FortranWords, Ftn_IO)
call extend(s:FortranWords, Ftn_F)
call extend(s:FortranWords, Ftn_Structure)
call extend(s:FortranWords, Ftn_Type)

fu! CapitalizeFortran()

	let CursorPos = getpos(".")
	let lnum = CursorPos[1]

	" if there is nothing or only space before the cursor then it come from a 'Enter'
	" in this case, go to the line above.
	" if it is already the 1st line then return false.
	let LineStr = getline( CursorPos[1] )
	let InputStr = strpart( LineStr, 0, CursorPos[2]-1 )
	let WordList = split( InputStr )
	if empty( WordList )
		if CursorPos[1] == 1
			return 0
		else
			let lnum = CursorPos[1] - 1
			let InputStr = getline( lnum )
			" return false if this line is also empty line
			let WordList = split( InputStr )
			if empty( WordList )
				return 0
			endif
			let InputIdx = strlen( InputStr ) - 1  
		endif
	else
		let lnum = CursorPos[1]
		let InputIdx = CursorPos[2] - 1 - 1
	endif

	"return if input char is not a delimiter
	let InputChar = strpart( InputStr, InputIdx, 1 )
	if InputChar !~ "[(, ]"
		return 0
	endif

	" return false if current input is within quotes
    if IsInQuote( InputStr )
        return 0
    endif
	
	" return false if current input is within comments
    if IsInComment( InputStr )
        return 0
    endif

	let DelimList = ['(', ',', ' ']
	let LastIdxList = []
	for delim in DelimList
		call add( LastIdxList, strridx( InputStr, delim, InputIdx-1 ) )
	endfor
	let LastDelimIdx = max( LastIdxList )
	let InputWord = strpart( InputStr, LastDelimIdx+1, InputIdx-LastDelimIdx-1 )
	let StrLeft   = strpart( InputStr, 0, LastDelimIdx+1 )

	for word in s:FortranWords
		if InputWord == word
			exe lnum . ',' . lnum . 
			  \ 's/' . InputStr . '/' . StrLeft . toupper(InputWord) . InputChar
		endif
	endfor

	call setpos( '.', CursorPos )

endf


fu! IsInQuote( InputStr )
"-------------------------------------------------------------------------------
" Check if current input char stays in quote
"-------------------------------------------------------------------------------
	let SearchBeginIdx = 0

    while 1

        " Find the index of first quote, single or double.
        " If there is no quote, return 0
        let SQuoteBeginIdx = stridx( a:InputStr, "'", SearchBeginIdx )
        let DQuoteBeginIdx = stridx( a:InputStr, '"', SearchBeginIdx )

        if SQuoteBeginIdx == -1 && DQuoteBeginIdx == -1
            return 0
        else
            let QuoteBeginIdx = min([SQuoteBeginIdx, DQuoteBeginIdx])
            if QuoteBeginIdx == -1
                let QuoteBeginIdx = max([SQuoteBeginIdx, DQuoteBeginIdx])
            endif
        endif

        " return true if reach the end of string
        if (QuoteBeginIdx + 1) == strlen( a:InputStr )
            return 1
        endif

        " Check if there is a pair quote.
        " Get the pair's index if exists, otherwise return 1
        let Quote = strpart( a:InputStr, QuoteBeginIdx, 1 )
        let PairQuoteIdx = stridx( a:InputStr, Quote, QuoteBeginIdx + 1 )
        if PairQuoteIdx == -1
            return 1
        else
            if (PairQuoteIdx + 1) == strlen( a:InputStr )
                return 0
            else
                let SearchBeginIdx = PairQuoteIdx + 1
            endif
        endif

    endwhile

endf	


fu! IsInComment( InputStr )
"-------------------------------------------------------------------------------
" Check if current input char stays in comment
" This only works when IsInQuote has been called first.
"-------------------------------------------------------------------------------

    let ExclLastIdx   = strridx( a:InputStr, '!' )

    " return false if there is no exclamation.
    if ExclLastIdx == -1
        return 0
    endif

    let SearchBeginIdx = strlen( a:InputStr ) - 1

    while 1

        let SQuoteLastIdx = strridx( a:InputStr, "'", SearchBeginIdx )
        let DQuoteLastIdx = strridx( a:InputStr, '"', SearchBeginIdx )
        let QuoteLastIdx  = max([SQuoteLastIdx, DQuoteLastIdx])

        " return true if exclamation is the last symbol among '"!
        if ExclLastIdx > QuoteLastIdx
            return 1
        endif

        " return false if exclamation is within a pair of quotes.
        let Quote        = strpart( a:InputStr, QuoteLastIdx, 1 )
        let QuotePairIdx = strridx( a:InputStr, Quote, QuoteLastIdx-1 )
        if QuotePairIdx < ExclLastIdx
            return 0
        endif

        let SearchBeginIdx = QuotePairIdx - 1

    endwhile

endf

autocmd CursorMovedI *.f90 call CapitalizeFortran()
